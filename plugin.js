//Product Categories Navigation Menu Scripts

$(document).ready(function(){
	
	$('.pccowboy-product-categories-nav-menu .parent-menu a').click(function () {
		//toggle current color
		if(!$(this).hasClass('current-category')){ 
			
			$(this).addClass('current-category');
			$(this).prev().addClass('hero-left');
			
		}
		
		
		$(this).prev().animate(
				{ 
					left: '10px',
					
				}, 
				300,'easeOutSine',
				function(){
					$(this).animate(
							{ 
								left: '-8px',
							}, 
							700, 'easeOutBounce'
					);
					
				}
		);//END $(this).prev().animate
		
		$('.pccowboy-product-categories-nav-menu .current-category').not(this).removeClass('current-category');
		$('.pccowboy-product-categories-nav-menu .xhero').not($(this).prev()).removeClass('hero-left');
	});

});