<?php
/*
Plugin Name: Product Categories Navigation Menu
Description: Widget : Display product categories navigation menu for sidebars. Supports multiple level menus.
Author: PcCowboy | Peter Nanayon
Version: 1.0
Author URI: http://pccowboy.x10.bz
*/


//class RandomPostWidget extends WP_Widget
class ProductCatNavMenuWidget extends WP_Widget
{ 
	
	
	
  function ProductCatNavMenuWidget()
  {
    $widget_ops = array('classname' => 'ProductCatNavMenuWidget', 'description' => 'Display product categories navigation menu for sidebars. Supports multiple level menus.' );
    $this->WP_Widget('ProductCatNavMenuWidget', 'PcCowboy: Product Categories Navigation Menu', $widget_ops);
  }//END CategorySidebarMenuWidget
 

  
  
  
  //diplay input data for widget
  function form($instance){
    $instance = wp_parse_args( (array) $instance, array( 'title' => '' ) );
    $title = $instance['title'];
?>
  <p><label for="<?php echo $this->get_field_id('title'); ?>">Top Title: <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo attribute_escape($title); ?>" /></label></p>
  
  
<?php    
    $args = array(
	    //'number'     => $number,
	    //'orderby'    => $orderby,
	    //'order'      => $order,
	    //'hide_empty' => $hide_empty,
	    //'include'    => $ids
	);
    
    
    $categories = get_terms( 'product_cat', $args );//get the categories 
      // returns an array of category objects
  }//END function form($instance)
  
  
  
  
  
  
  //update widgets data function
  function update($new_instance, $old_instance)
  {
    $instance = $old_instance;
    $instance['title'] = $new_instance['title'];
  
    return $instance;
  }//END function update($new_instance, $old_instance)
  
  
  
  
  
  
  	//helper function to display the menu
	function echo_menu($categories,$listWrapStart = '<ul>', $listWrapEnd = '</ul>', $level = 0){
		?>
		<?php echo $listWrapStart ;?>
			    		
			<?php foreach($categories as $category) : ?>
			<li>
				<?php
	    		$subArgs = array(
				    //'number'     => $number,
				    //'orderby'    => $orderby,
				    //'order'      => $order,
				    //'hide_empty' => $hide_empty,
				    //'include'    => $ids
				    'orderby' => 'name',
			    	'order' => 'ASC',
			    	'parent' => $category->term_id,
			    	'hide_empty' => 0,
			    	
				);
				
				$sub_categ = get_terms( 'product_cat', $subArgs );
				
				//#143
				//this following codes is to check if the current category or current parent category 
			    //should be uncollapsed.
				
				//current categ flag
			    $is_current_categ = false;
			    $is_children_current = false;
			    $current_categ2 = false;
			    
				
			    //class in will be added if true
			    $collapseIn = "";
			    $current_categ_class = "";
			    $hero = "";
			    
			    //get current category
			    $current_categ = get_queried_object();
			    
			    //get the children of current categoruy
			    $termchildren = get_term_children( $category->term_id, $category->taxonomy);
			    
				//search for match
			    foreach($termchildren as $tempTerm){
			    	if($tempTerm == $current_categ->term_id){
			    		//found it, current category is children of this loop category
			    		$is_current_categ = true;
			    		$is_children_current = true;
			    		break;//found it // exit
			    	}
			    }
			    
				
			    //if current category is the current loop category
			    if($current_categ->term_id == $category->term_id){
			    	$is_current_categ = true;
			    	$current_categ2 =  true;
			    }
			    
			    
			    
			    
			    
			    //determine if should be collpase or not
			    if( $is_current_categ) :
			   		$collapseIn = " in";
			    	$current_categ_class = ' current-category';
			    	
			    endif;
			    
			    if(!$is_children_current && $current_categ2) :
			    	$hero = " hero-left";
			    endif;
			    
			    //END #143
				
				?>
				
				
				<?php if( !is_a($sub_categ,'WP_Error' ) && is_array($sub_categ) && !empty($sub_categ) ) : ?>
			
			    	<div class="xhero <?php echo $hero?>" style=""></div>
			    	<a
						ajaxurl="<?php echo get_term_link($category->term_id,'product_cat');?>"
						ajaxurl-title="<?php bloginfo('name'); ?> - <?php  echo $category->name; ?>"
						class="accordion-heading<?php //echo $current_categ_class; ?>"
						data-toggle="collapse"
						data-target="#sub<?php echo $category->term_id; ?>"
					>
	    				<?php if ( $level & 1 ) : ?>
	    					<i class="fa fa-circle-o-notch" aria-hidden="true"></i>
	    				<?php else : ?>
	    					<i class="fa fa-circle" aria-hidden="true"></i>
	    				<?php endif;?>
	    				
	    	
			    		<span class="nav-header-primary" ><?php  echo $category->name; ?></span>
			    		<i class="fa fa-chevron-right link-icon" aria-hidden="true"></i>
			    			
			    	</a>
			    	
			    	<?php $this->echo_menu($sub_categ, '<ul class="nav nav-list p collapse'. $collapseIn .'" id="sub'. $category->term_id .'">', '</ul>', $level + 1); ?>
			    	
				<?php else : ?>
					<div class="xhero <?php echo $hero?>" style=""></div>
					<a 
						ajaxurl="<?php echo get_term_link($category->term_id,'product_cat');?>"
						ajaxurl-title="<?php bloginfo('name'); ?> - <?php  echo $category->name; ?>"
						class="<?php echo $current_categ_class; ?>"
					>
	    			
			    		<?php if ($level & 1 ) : ?>
	    					<i class="fa fa-circle-o-notch" aria-hidden="true"></i>
	    				<?php else : ?>
	    					<i class="fa fa-circle" aria-hidden="true"></i>
	    				<?php endif;?>
			    		<span><?php  echo $category->name ?></span>
			    			
			    	</a>
			    
				<?php endif; ?>
				
			</li>
			    			
			<?php endforeach;?>
			    		
			    		
		<?php echo $listWrapEnd ;?>
	<?php 
	
	}//function echo_menu(
	
	
	
	
	
  
   
  function widget($args, $instance)
  {
    extract($args, EXTR_SKIP);
 
    echo $before_widget;
    
    
    $title = empty($instance['title']) ? ' ' : apply_filters('widget_title', $instance['title']);
 
    if (!empty($title)){
    	echo $before_title . $title . $after_title;
    }
    
    $args = array(
	    'orderby' => 'name',
    	'order' => 'ASC',
    	'parent' => 0,
    	'hide_empty' => 0,
    	
	);
     
    $categories = get_terms( 'product_cat', $args );//get the categories 
 	?>
    
    <nav class="pccowboy-product-categories-nav-menu ">
    
    	<?php if( !is_a($categories,'WP_Error' ) ) : ?>
    		<?php $this->echo_menu($categories, '<ul class="parent-menu nav nav-list">'); ?>
    	<?php endif;?>
    	
    </nav>
     
 	<?php 
 	
 	
 	
 	
 	
    echo $after_widget;
  }//END func widget
  
  
	
  
  
  
 
}
add_action( 'widgets_init', create_function('', 'return register_widget("ProductCatNavMenuWidget");') );


wp_enqueue_style( 'ProductCatNavMenuWidget Styles', plugin_dir_url(__FILE__) . 'style.css');
wp_enqueue_script( 'ProductCatNavMenuWidget Scripts', plugin_dir_url(__FILE__) . 'plugin.js');

?>
